//
//  DetailViewController.swift
//  PadiReview
//
//  Created by Michele Navolio on 04/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    
    @IBOutlet var immagineImmersione: UIImageView!
    @IBOutlet var nomeImmersione: UILabel!
    @IBOutlet var numeroImmersione: UILabel!
    @IBOutlet var myView: UIView!
    
    
    @IBOutlet var editButton: UIBarButtonItem!
    var immersione : ImmersioneModel?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        DataManager.shared.DetView = self
        
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")

        title = immersione?.name
        
        nomeImmersione.text = immersione?.name
        immagineImmersione.image = immersione?.immaginePronta
        numeroImmersione.text = "\(immersione!.numeroImmersione)"
        immagineImmersione.layer.cornerRadius = 20.0
        
        
        //trucco per dare profondità
        myView.layer.cornerRadius = 20.0
        myView.layer.shadowColor = UIColor.gray.cgColor
        myView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        myView.layer.shadowRadius = 12.0
        myView.layer.shadowOpacity = 0.7

        
    }
    
    
    @IBAction func editButtonPressed(_ sender: UIBarButtonItem) {
        
    self.performSegue(withIdentifier: "edit", sender: immersione)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit" {
            let controller = segue.destination as! AddViewController
            
            controller.immersione = sender as? ImmersioneModel

            controller.title = immersione?.name
            
        }
    }

    
}
