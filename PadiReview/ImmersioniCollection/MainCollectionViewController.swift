//
//  MainCollectionViewController.swift
//  PadiReview
//
//  Created by Michele Navolio on 04/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit


class MainCollectionViewController: UICollectionViewController {
    
    
    @IBOutlet var addButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataManager.shared.CollView = self
        DataManager.shared.caricaDati()
        //        navigationItem.title = "Home"
        
        navigationItem.titleView = UIImageView(image: UIImage(named: "padi-logo"))
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail" {
            let controller = segue.destination as! DetailViewController
            
            let cell = sender as! CollectionViewCell
            if let indexPath = self.collectionView!.indexPathForItem(at: cell.center) {
                
                controller.immersione = DataManager.shared.array[indexPath.row]
                
            }
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        
        return DataManager.shared.array.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        cell.label.text = DataManager.shared.array[indexPath.row].name
        cell.number.text = "\(DataManager.shared.array[indexPath.row].numeroImmersione)"
        cell.immagine.image = DataManager.shared.array[indexPath.row].immaginePronta
        cell.immagine.layer.cornerRadius = 10

        cell.layer.shadowColor = UIColor.systemGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 4, height: 4)
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 5
        
        cell.layer.cornerRadius = 10


        
        return cell
        
    }
    
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        
        
    }
    
    
}
