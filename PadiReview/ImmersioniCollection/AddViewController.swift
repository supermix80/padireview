//
//  AddViewController.swift
//  PadiReview
//
//  Created by Michele Navolio on 12/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class AddViewController: UIViewController {
    
    var immersione : ImmersioneModel?
    
    @IBOutlet var nomeImmersione: UILabel!
    @IBOutlet var immagineImmersione: UIImageView!
    @IBOutlet var numeroImmersione: UILabel!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        if immersione == nil {
            print("crea immersione")
//            DataManager.shared.newImmersione(name: <#T##String#>, numeroImmersione: <#T##Int#>, immagine: <#T##UIImage#>)
        } else {
            print("Sto modificando \(String(describing: immersione?.name))")
            nomeImmersione.text = immersione?.name
            numeroImmersione.text = immersione?.numeroImmersione.description
            immagineImmersione.image = immersione?.immaginePronta
        }
//        se arrivo da edit, devo modificare l'oggetto di provenienza

        
//        se arrivo da nuova, devo creare una nuova immersione
        // Do any additional setup after loading the view.
        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
