//
//  YDViewController.swift
//  PadiReview
//
//  Created by Michele Navolio on 16/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class YDViewController: UIViewController {

    @IBOutlet var labelDettaglio: UILabel!
    @IBOutlet var imageDettaglio: UIImageView!
    
    var testo = ""
    var imageTemp : UIImage?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        labelDettaglio.text = testo
        imageDettaglio.image = imageTemp
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
