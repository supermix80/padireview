//
//  Y3TableViewCell.swift
//  PadiReview
//
//  Created by Michele Navolio on 14/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class Y3TableViewCell: UITableViewCell {
    
    @IBOutlet var titoloSezione3: UILabel!
    
    @IBOutlet var collectionView3: UICollectionView!
    
    
    var array3 : [String] = ["Pro-Check","E-Cards","Travel"]
    var arrayImage3 : [UIImage] = [UIImage(named: "Pro-Check")!, UIImage(named: "E-Cards")!, UIImage(named: "Travel")!]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView3.delegate = self
        collectionView3.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension Y3TableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        array3.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell3", for: indexPath) as! Y3CollectionViewCell
        
        cell.labelCollection3.text = array3[indexPath.row]
        cell.imageCollection3.image = arrayImage3[indexPath.row]
        
        cell.layer.shadowColor = UIColor.systemGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 4, height: 4)
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 5
        cell.layer.cornerRadius = 10
        
        return cell
    }
    
    
}
