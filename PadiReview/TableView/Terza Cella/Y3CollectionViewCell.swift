//
//  Y3CollectionViewCell.swift
//  PadiReview
//
//  Created by Michele Navolio on 14/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class Y3CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageCollection3: UIImageView!
    
    @IBOutlet var labelCollection3: UILabel!
    
}
