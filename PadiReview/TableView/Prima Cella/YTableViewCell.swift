//
//  YTableViewCell.swift
//  PadiReview
//
//  Created by Michele Navolio on 13/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class YTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet var titoloSezione: UILabel!
    @IBOutlet var collectionView1: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView1.delegate = self
        collectionView1.dataSource = self
        DataManager.shared.primaCellaView = self
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
    }
    
    
    
}

extension YTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.shared.arrayPrimaCella.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! Y1CollectionViewCell
        
        
        cell.titoloCollection.text = DataManager.shared.arrayPrimaCella[indexPath.row]
        cell.imageCollection.image = DataManager.shared.arrayImagePrimaCella[indexPath.row]
        
        cell.imageCollection.layer.cornerRadius = 10
        
        cell.layer.shadowColor = UIColor.systemGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 4, height: 4)
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 5
        cell.layer.cornerRadius = 10
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("ho toccato \(indexPath.row)")
        
        //        let index = indexPath.row
        
    }
    
    //    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
    //        print("high\(indexPath.row)")
    //        prepareForReuse()
    //    }
    //
    
    
    
}

