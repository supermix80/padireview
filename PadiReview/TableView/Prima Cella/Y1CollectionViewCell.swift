//
//  1CollectionViewCell.swift
//  PadiReview
//
//  Created by Michele Navolio on 13/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class Y1CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var titoloCollection: UILabel!
    
    @IBOutlet var imageCollection: UIImageView!
    
}
