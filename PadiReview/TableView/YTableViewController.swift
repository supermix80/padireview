//
//  YTableViewController.swift
//  PadiReview
//
//  Created by Michele Navolio on 13/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class YTableViewController: UITableViewController {
    
    var arrayTitoli : [String] = ["Tools","Training","Extra"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleView = UIImageView(image: UIImage(named: "padi-logo"))
         self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        
        DataManager.shared.HomeView = self

 
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cella1", for: indexPath) as? YTableViewCell else {
                fatalError("Error")
            }
            
            cell.titoloSezione.text = DataManager.shared.arrayTitoliSezioni[indexPath.row]
            
            print(indexPath.row)
            

            
            return cell
            
        } else if indexPath.row == 1{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cella2", for: indexPath) as? Y2TableViewCell else {
                fatalError("Error")
            }
            
            cell.titoloSezione2.text = DataManager.shared.arrayTitoliSezioni[indexPath.row]

            
            return cell
            
        } else {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cella3", for: indexPath) as? Y3TableViewCell  else {
                fatalError("Error")
            }
            
            cell.titoloSezione3.text = DataManager.shared.arrayTitoliSezioni[indexPath.row]
            return cell

        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print("ho toccato da view controller")
        
        if segue.identifier == "yd" {
                  let controller = segue.destination as! YDViewController
                  
                  let cell = sender as! Y1CollectionViewCell
                  
            controller.title = cell.titoloCollection.text
            controller.testo = cell.titoloCollection.text!
            controller.imageTemp = cell.imageCollection.image
        }
                    
    }
    
}
