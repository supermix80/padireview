//
//  Y2CollectionViewCell.swift
//  PadiReview
//
//  Created by Michele Navolio on 13/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class Y2CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet var titoloCollection2: UILabel!
    @IBOutlet var immagineCollection2: UIImageView!
    
}
