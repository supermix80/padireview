//
//  Y2TableViewCell.swift
//  PadiReview
//
//  Created by Michele Navolio on 13/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class Y2TableViewCell: UITableViewCell {
    
    
    
    var array2 : [String] = ["Addestramento","Signs"]
    var arrayImage2 : [UIImage] = [UIImage(named: "Addestramento")!, UIImage(named: "Signs")! ]
    
    @IBOutlet var titoloSezione2: UILabel!
    
    @IBOutlet var collectionView2: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView2.delegate = self
        collectionView2.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
    }
    
}

extension Y2TableViewCell : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell2", for: indexPath) as! Y2CollectionViewCell
        
        cell.titoloCollection2.text = array2[indexPath.row]
        cell.immagineCollection2.image = arrayImage2[indexPath.row]
        
        cell.layer.shadowColor = UIColor.systemGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 4, height: 4)
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 5
        cell.layer.cornerRadius = 10
        
        
        return cell
    }
    
    
    
    
}
