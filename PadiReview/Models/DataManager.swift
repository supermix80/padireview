//
//  DataManager.swift
//  PadiReview
//
//  Created by Michele Navolio on 04/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit

class DataManager {
    
    static let shared = DataManager()
    
    var array : [ImmersioneModel] = []
    typealias Array = [ImmersioneModel]
    
    var CollView : MainCollectionViewController?
    var DetView : DetailViewController?
    
    var arrayTitoliSezioni : [String] = ["Tools","Training","Extra"]
    var HomeView : YTableViewController?
    
    var arrayPrimaCella : [String] = ["Attrezzatura","Nodi", "Check List"]
    var arrayImagePrimaCella : [UIImage] = [UIImage(named: "Bombole")!, UIImage(named: "Nodi")!, UIImage(named: "CheckList")! ]
    var primaCellaView : YTableViewCell?

  
    func caricaDati() {
        
        if let data = UserDefaults.standard.data(forKey: "salva") {
 
            do {
                let decoder = PropertyListDecoder()
                array = try decoder.decode(Array.self, from: data)
            } catch {
                debugPrint(error.localizedDescription)
            }
        } else {
            // se entra qui è il primo lancio, quindi faccio esempi
            let sharm = ImmersioneModel(name: "Sharm", numeroImmersione: 1, immagine: #imageLiteral(resourceName: "sharm").pngData())
            let maldive = ImmersioneModel(name: "Maldive", numeroImmersione: 2, immagine: #imageLiteral(resourceName: "maldive").pngData())
            let sharm1 = ImmersioneModel(name: "Sharm2", numeroImmersione: 3, immagine: #imageLiteral(resourceName: "sharm").pngData())
            let maldive1 = ImmersioneModel(name: "Maldive2", numeroImmersione: 4, immagine: #imageLiteral(resourceName: "maldive").pngData())
            let sharm2 = ImmersioneModel(name: "Sharm3", numeroImmersione: 5, immagine: #imageLiteral(resourceName: "sharm").pngData())
            let maldive2 = ImmersioneModel(name: "Maldive3", numeroImmersione: 6, immagine: #imageLiteral(resourceName: "maldive").pngData())
            let sharm3 = ImmersioneModel(name: "Sharm4", numeroImmersione: 7, immagine: #imageLiteral(resourceName: "sharm").pngData())
            let maldive3 = ImmersioneModel(name: "Maldive4", numeroImmersione: 8, immagine: #imageLiteral(resourceName: "maldive").pngData())
            let sharm4 = ImmersioneModel(name: "Sharm5", numeroImmersione: 9, immagine: #imageLiteral(resourceName: "sharm").pngData())
            let maldive4 = ImmersioneModel(name: "Maldive5", numeroImmersione: 10, immagine: #imageLiteral(resourceName: "maldive").pngData())
            let sharm5 = ImmersioneModel(name: "Sharm6", numeroImmersione: 11, immagine: #imageLiteral(resourceName: "sharm").pngData())
            let maldive5 = ImmersioneModel(name: "Maldive6", numeroImmersione: 12, immagine: #imageLiteral(resourceName: "maldive").pngData())
            
            // le mettiamo nell'array
            array = [sharm, maldive, sharm1, maldive1, sharm2, maldive2, sharm3, maldive3, sharm4, maldive4, sharm5, maldive5]
            
            // e salvo, così le crea una volta sola
            salva()
            
        }
    }
    
    func newImmersione(name: String, numeroImmersione: Int, immagine : UIImage) {
        let immersione = ImmersioneModel(name: name, numeroImmersione: 0, immagine: immagine.pngData())
        
        array.insert(immersione, at: 0)
        
        salva()
        
        CollView?.collectionView.reloadData()
        
    }
    
    func salva() {
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        do {
            let data = try encoder.encode(array)
            UserDefaults.standard.set(data, forKey: "salva")
        } catch {
            debugPrint(error.localizedDescription)
        }
    }
    
    
}


