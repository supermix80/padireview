//
//  Immersione.swift
//  PadiReview
//
//  Created by Michele Navolio on 04/12/2019.
//  Copyright © 2019 Michele Navolio. All rights reserved.
//

import UIKit


class ImmersioneModel: Codable {
    var name : String
    var numeroImmersione : Int
    var immagine : Data?
    
    var immaginePronta : UIImage? {
        return UIImage(data: immagine ?? Data())
    }

    init(name: String, numeroImmersione: Int, immagine : Data?) {
    self.name = name
    self.numeroImmersione = numeroImmersione
    self.immagine = immagine

}
    
    
    enum CodingKeys: String, CodingKey {
        case name
        case numeroImmersione
        case immagine
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.numeroImmersione = try container.decode(Int.self, forKey: .numeroImmersione)
        self.immagine = try container.decode(Data.self, forKey: .immagine)

    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(numeroImmersione, forKey: .numeroImmersione)
        try container.encode(immagine, forKey: .immagine)

    }
}

